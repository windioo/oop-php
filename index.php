<?php 

require 'animal.php';
require 'Frog.php';
require 'Ape.php';

$sheep = new Animal("shaun");

echo "Nama Hewan : ".$sheep->name; // "shaun"
echo "<br>";
echo "Jumlah Kaki : ".$sheep->legs; // 2
echo "<br>";
echo "Jenis Hewan Berdarah Dingin: ".$sheep->cold_blooded; // false
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Jumlah Kaki  kera sakti: ".$sungokong->legs; // 2
echo "<br>";
echo "Suara kera sakti : ";
$sungokong->yell() ;// "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>";
echo "Jumlah Kaki  katak : ".$kodok->legs; // 2
echo "<br>";
echo "Lompatan katak : ";
$kodok->jump() ; // "hop hop"

?>